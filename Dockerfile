FROM ubuntu:18.04

MAINTAINER Honza Hommer <honza@hommer.cz>

# Install the application.
ENV DEBIAN_FRONTEND noninteractive
RUN export DEBIAN_FRONTEND=noninteractive
RUN update-locale LANG="C.UTF-8"
RUN dpkg-reconfigure locales
RUN echo 'Acquire::ForceIPv4 "true";' > /etc/apt/apt.conf.d/99force-ipv4
RUN apt-get -yq update
RUN apt-get -yq install git gdal-bin python-gdal libgdal-dev python3-rtree python-pip unar
RUN apt-get -y autoremove
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/partial/* /tmp/* /var/tmp/*

# Externally accessible data is by default put in /data
WORKDIR /data
VOLUME ["/data"]

# Output version and capabilities by default.
CMD gdalinfo --version && gdalinfo --formats && ogrinfo --formats
